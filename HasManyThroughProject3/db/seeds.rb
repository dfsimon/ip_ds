# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])(name: 'Emanuel', city: cities.first)
#   Mayor.create

patients = Patient.create([{ name: 'Jerry Waldo' }])
patients = Patient.create([{ name: 'Chris Craigs' }])
patients = Patient.create([{ name: 'Laura Red' }])

physicians = Physician.create([{ name: 'Johnny Brake' }])
physicians = Physician.create([{ name: 'Tommy Gun' }])
physicians = Physician.create([{ name: 'Lewis Louis' }])


Appointment.create(physician: 'Johnny Brake', patient: 'Jerry Waldo', reason: 'Feeling sick',
                   appointment_date: '09-10-2016', appointment_time: '8:30 AM', notes: 'Gave medicine', appt_completion: 'true')

Appointment.create(physician: 'Tommy Gun', patient: 'Chris Craigs', reason: 'Need to lose weight',
                   appointment_date: '12-10-2016', appointment_time: '10:30 AM', notes: 'Gave supplements', appt_completion: 'false')

Appointment.create(physician: 'Lewis Louis', patient: 'Laura Red', reason: 'Stomach hurts',
                   appointment_date: '08-10-2014', appointment_time: '2:30 PM', notes: 'Gave stomach stuff', appt_completion: 'true')

