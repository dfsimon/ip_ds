class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.integer :diagnostic_code_id
      t.string :diagnostic_code

      t.timestamps
    end
  end
end
