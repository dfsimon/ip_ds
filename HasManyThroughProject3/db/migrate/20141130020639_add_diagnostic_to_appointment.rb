class AddDiagnosticToAppointment < ActiveRecord::Migration
  def change
      add_column :appointments, :diagnostic_id, :decimal
  end
end
