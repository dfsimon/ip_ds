class ModAppointments3 < ActiveRecord::Migration
  def change
    remove_column :appointments, :fee
    remove_column :appointments, :diagnostic_id
  end
end
