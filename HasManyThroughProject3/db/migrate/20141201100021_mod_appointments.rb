class ModAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :appointment_time, :time
    change_column :appointments, :appointment_date, :date
  end
end
