class ModifyDiagnostic4 < ActiveRecord::Migration
  def change
      drop_table :diagnostic_codes

      create_table :diagnostic_codes do |t|
        t.string :diagnostic_code_id
        t.string :diagnostic_code

        t.timestamps
      end
    end
end
