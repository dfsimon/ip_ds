class ModifyAppointmentDate < ActiveRecord::Migration
  def change
    change_column :appointments, :appointment_date, :datetime
  end
end
