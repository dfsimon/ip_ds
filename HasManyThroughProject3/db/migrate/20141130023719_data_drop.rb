class DataDrop < ActiveRecord::Migration
  def change
    Patient.destroy_all
    Physician.destroy_all
    Appointment.destroy_all
  end
end
