class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :physician_id
      t.integer :patient_id
      t.text :reason
      t.date :appointment_date
      t.integer :notes

      t.timestamps
    end
  end
end
