class AddDiagnosticToAppointment2 < ActiveRecord::Migration
  def change
    add_column :appointments, :diagnostic_code_id, :decimal
  end
end
