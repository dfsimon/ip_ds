class ModifyAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :reason, :string
    add_column :appointments, :notes, :string
    remove_column :appointments, :complaint
  end
end