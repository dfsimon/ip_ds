class AddCompletionStatus < ActiveRecord::Migration
  def change
    add_column :appointments, :appt_completion, :boolean
  end
end
