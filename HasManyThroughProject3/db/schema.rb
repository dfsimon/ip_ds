# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141203034223) do

  create_table "appointments", force: true do |t|
    t.integer  "patient_id"
    t.date     "appointment_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "physician_id"
    t.string   "reason"
    t.string   "notes"
    t.decimal  "diagnostic_code_id"
    t.boolean  "appt_completion"
    t.string   "appointment_time"
  end

  create_table "diagnostic_codes", force: true do |t|
    t.decimal  "diagnostic_code_id"
    t.string   "diagnostic_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insurances", force: true do |t|
    t.string   "name"
    t.string   "street_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.integer "invoice_id"
  end

  create_table "patients", force: true do |t|
    t.string   "name"
    t.string   "street_address"
    t.integer  "insurance_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physicians", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
