require 'test_helper'

class ModifyAppointmentsControllerTest < ActionController::TestCase
  setup do
    @modify_appointment = modify_appointments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:modify_appointments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create modify_appointment" do
    assert_difference('ModifyAppointment.count') do
      post :create, modify_appointment: {  }
    end

    assert_redirected_to modify_appointment_path(assigns(:modify_appointment))
  end

  test "should show modify_appointment" do
    get :show, id: @modify_appointment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @modify_appointment
    assert_response :success
  end

  test "should update modify_appointment" do
    patch :update, id: @modify_appointment, modify_appointment: {  }
    assert_redirected_to modify_appointment_path(assigns(:modify_appointment))
  end

  test "should destroy modify_appointment" do
    assert_difference('ModifyAppointment.count', -1) do
      delete :destroy, id: @modify_appointment
    end

    assert_redirected_to modify_appointments_path
  end
end
