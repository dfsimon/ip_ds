json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :physician_id, :patient_id, :notes, :appointment_date, :appointment_time, :diagnostic_code, :reason, :appt_completion
  json.url appointment_url(appointment, format: :json)
end
