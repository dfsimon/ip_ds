json.extract! @appointment, :id, :physician_id, :patient_id, :reason, :appointment_date, :appointment_time, :diagnostic_code_id, :notes, :appt_completion, :created_at, :updated_at
