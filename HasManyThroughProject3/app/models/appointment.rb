class Appointment < ActiveRecord::Base
  belongs_to :specialist
  belongs_to :physician
  belongs_to :patient
  belongs_to :diagnostic_code

  #validates_presence_of :fee
  #validates_numericality_of :fee, :greater_than => 0

  #This method will calculate the amount of time before appointment
  #If less than 8 hours, a validation box will prompt user and not allow them to cancel
  #If 8 hours or greater, validation box will ask are you sure.  Upon yes selection, appointment destroyed
  def cancel_appointment

    #     if appointment_date = Day.today
    #     @time = Time.now
    #     @appt_time = appointment_time.to_i
    #     @appt_time - @time = @numhourstill
    #     if @numhourstill < 8
    #     validation you can't cancel
    #     end
    #     else
    #     cancel
    #     end

  end

  def list_appointments
    @slots_per_day = 17
  end

  def appts_completed
    appointment = Appointment.where(appt_completion: true)
  end


end
